package industry.coderocks.com.industry;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by renkixeon on 4/25/2016 AD.
 */
public class buyTicket extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buyticket);

        ImageView buyticket = (ImageView)findViewById(R.id.buyticket);

        buyticket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getBaseContext(),ResultActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
